using System.Collections;
using System.Collections.Generic;

using UnityEngine;

[RequireComponent(typeof(AudioSource))]
public class SFXManager : MonoBehaviour
{
    private  AudioSource audioSource;
    void OnEnable(){
        Session.Instance.events.onSoundPlay.AddListener((clip)=>Play(clip));
    }
    void Start()
    {
        audioSource = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Play(AudioClip clip)
    {
        audioSource.PlayOneShot(clip);
    }
}


