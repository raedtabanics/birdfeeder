using System.Collections;
using System.Collections.Generic;

using UnityEngine;

using DG.Tweening;
[RequireComponent(typeof(AudioSource))]
public class SmoothVolume : MonoBehaviour
{
    private AudioSource audioSource;
    public AnimationCurve smooth;
    public float duration;

    public AudioClip chirpSFX;
    void Start()
    {
        audioSource = GetComponent<AudioSource>();
        Session.Instance.events.onSoundPlay.Invoke(chirpSFX);
        float delta = 0;
        DOTween.To(() => delta, x => delta = x, 1, duration)
        .OnUpdate(() => {
        audioSource.volume = smooth.Evaluate(delta);
    });
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
