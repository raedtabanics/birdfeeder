using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Linq;

using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;


public class Auth : MonoBehaviour{
    


        //-Signin Variables
        private Transform login;
        private Button loginBtn;
        
        private InputField username;
        private Transform msg;
        private Text msgText;

        private Button play;
        private bool ready = false;
        async void Start(){
            
            login = transform.Find("Login");
            loginBtn = transform.Find("Login/Guest").GetComponent<Button>();
            username = login.transform.Find("UserName").GetComponent<InputField>();
            play = transform.Find("Play").GetComponent<Button>();

            play.onClick.AddListener(()=>Play());
            //msg = login.transform.Find("MSG");
            //msgText = msg.Find("Text").GetComponent<Text>();

            loginBtn.onClick.AddListener(()=>LoginAnon());

            await Task.Run(async ()=>{
                while(!Session.Instance.ready)
                    await Task.Delay(25);
                return true;
            });

            Debug.Log("[Auth is Ready]");
            if(Session.Instance.Auth.CurrentUser == null){
                TweenHelper.DoFade(login,1,1);    
                return;
            }
                
            Session.Instance.User = Session.Instance.Auth.CurrentUser;
            Session.Instance.username = Session.Instance.User.UserId;
            await Session.Instance.GetPlayer();
            ready = true;
            
        }


        private async void LoginAnon(){
            
            if(username.text.Length==0){
                msgText.text ="Please enter username...";
                TweenHelper.DoPop(msg,Vector3.one,.3f);  
                Invoke("HideMsg",1);
                return;
            }
            string proposedUserName = username.text;
            
             
            Session.Instance.User = await Session.Instance.Auth.SignInAnonymouslyAsync();
            Session.Instance.username = Session.Instance.User.UserId;
            await Session.Instance.InjectPlayer(new PlayerModel());
            await Session.Instance.GetPlayer();
            ready= true;
            
        }
        
        private void HideMsg(){
            TweenHelper.DoPop(msg,Vector3.zero,.3f); 
        }

        private void Play(){
            if(!ready)
                return;
            //Session.Instance.events.onUserAuthenticated.Invoke();
            SceneManager.LoadScene(1);
        }
    
}
