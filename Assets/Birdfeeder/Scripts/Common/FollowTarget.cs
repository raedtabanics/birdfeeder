using System.Collections;
using System.Collections.Generic;

using UnityEngine;

public class FollowTarget : MonoBehaviour
{
    public AnimationCurve smooth;
    public Vector3 offset;
    public Transform target;
    public float damp;
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if(target == null)
            return;
        var distance = Vector3.Distance(transform.position, target.position)/damp;
        transform.position = Vector3.Lerp(transform.position,target.position+offset,smooth.Evaluate(distance)*Time.fixedDeltaTime);
        //transform.LookAt(target.position + focus);
    }
}
