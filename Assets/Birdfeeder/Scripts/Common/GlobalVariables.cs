using System.Collections;
using System.Collections.Generic;

using UnityEngine;

public static class GlobalVariables 
{
    public static Key key;
    public static string Gold = "gold";
    public static string Hearts = "hearts";
    public static string Buildings = "buildings";

}

public enum Key{
    GOLD,
    HEARTS
}


