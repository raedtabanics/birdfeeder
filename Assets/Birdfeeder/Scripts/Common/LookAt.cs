using System.Collections;
using System.Collections.Generic;

using UnityEngine;

public class LookAt : MonoBehaviour
{
    public Transform target;
    void Setup()
    {
        target = Camera.main.transform;
    }

    // Update is called once per frame
    void Update()
    {
        if(target == null)
            Setup();
        transform.LookAt(target);
    }
}
