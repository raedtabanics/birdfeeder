using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;

using UnityEngine;
using UnityEngine.UI;

using DG.Tweening;
public static class TweenHelper 
{
    public static void DoPop(Transform obj,Vector3 value, float duration){
        obj.DOScale(value,duration);
    }
    public static async Task DoSlideX(Transform obj,float value, float duration){
        await obj.DOLocalMoveX(value,duration).AsyncWaitForCompletion();
    }
    public static async Task DoAnchorSlideX(Transform obj,float value, float duration){
        RectTransform rect = obj.GetComponent<RectTransform>();
        if(rect == null)
            return;
        await rect.DOAnchorPosX(value,duration).AsyncWaitForCompletion();
    }
    public static async Task DoSlideY(Transform obj,float value, float duration){
        await obj.DOLocalMoveY(value,duration).AsyncWaitForCompletion();
    }
    public static async Task DoFade(Transform obj,float value, float duration){
        CanvasGroup cg = obj.GetComponent<CanvasGroup>();
        if(cg == null)
            return;
        await cg.DOFade(value,duration).AsyncWaitForCompletion();
    }
}
