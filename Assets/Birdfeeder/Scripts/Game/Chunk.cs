using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;

using UnityEngine;

using DG.Tweening;
public class Chunk : MonoBehaviour
{
    private Transform buildings;
    private Transform land;
    private Transform green;

    public float delay;
    public int delay2;
    public float by;
    public float ly;
    public float gy;
    
    public void Start(){
        land = transform.Find("Land");
        buildings = transform.Find("Building");
        green = transform.Find("Green");

        //Spawn();
    }

    public async Task<bool> Spawn(){
        /*
        await land.DOMoveY(ly,delay).AsyncWaitForCompletion();
        await buildings.DOMoveY(by,delay).AsyncWaitForCompletion();
        await green.DOMoveY(gy,delay).AsyncWaitForCompletion();
        */
        land.DOMoveY(ly,delay);
        await Task.Delay(delay2);
        buildings.DOMoveY(by,delay);
        await Task.Delay(delay2);
        green.DOMoveY(gy,delay);
        await Task.Delay(delay2);

        return true;
    }
}
