using System.Collections;
using System.Collections.Generic;

using UnityEngine;

using DG.Tweening;
public class Boat : MonoBehaviour,IInteractable
{
    //-Anim Variables
    public AnimationCurve smooth;
    public float Amplitude;
    public float damp;
    private Vector3 position;
    private float timer;
    private bool tweening = false;


    //-Logic Variables 
    public int amount = 100;
    public int times = 5;

    void Start()
    {
        position = transform.position;
    }

    // Update is called once per frame
    void Update()
    {
        timer +=Time.deltaTime*damp;
        transform.position = position + Vector3.up*smooth.Evaluate(timer%1)*Amplitude*Time.fixedDeltaTime ;
        
    }

    public async void Interact(){
        if(tweening)
            return;
        if(times <=0)
            return;
        tweening = true;
        
        await StatsHelper.Add(Currency.GOLD,amount);
        await transform.DOPunchScale(Vector3.up*.1f,.5f).AsyncWaitForCompletion();
        
        times--;
        tweening = false;

    }
}
