using System.Threading.Tasks;

using UnityEngine;
using UnityEngine.UI;

using DG.Tweening;
public class Building : MonoBehaviour,IInteractable
{
    public Currency curreny;
    public BuildingModel model;
    private bool ready = false;
    private float progress =0 ;

    public GameObject currencyIcon;
    public GameObject progressbar;
    public Image fill;
    public ParticleSystem currencyPS;
    public ParticleSystem upgradePS;

    public Transform mainBuilding;
    private bool tweening =false;

    public AudioClip coinSFX;
    public AudioClip updateSFX;

    void OnEnable(){
        Session.Instance.events.onBuildingUpgrade.AddListener((assetId)=>OnUpgrade(assetId));
    }

    public void Setup(BuildingModel model){
        this.model = model;
    }

    // Update is called once per frame
    void Update()
    {
        if(ready && !model.autoharvest)
            return;
        if(ready && model.autoharvest)
            Harvest();


        progress +=Time.deltaTime;
        fill.fillAmount = progress/model.time;

        if(progress>= model.time){
            currencyIcon.SetActive(true);
            progressbar.SetActive(false);
            ready=true;
        }
            
    }
    public async void Interact(){

        if(!ready)
            return;
        Harvest();
        await PlayAnim();
    }

    public async void Harvest(){
        currencyPS.Play();
        currencyIcon.SetActive(false);
        progress = 0;
        ready = false;
        progressbar.SetActive(true);

        Session.Instance.events.onSoundPlay.Invoke(coinSFX);

        await StatsHelper.Add(curreny,model.amount);
    }

    private async void OnUpgrade(string assetId){
        if(!model.assetId.Equals(assetId))
            return;
        upgradePS.Play();
        await PlayAnim();
    }

    //-Animation Methods
    private async Task<bool> PlayAnim(){
        await mainBuilding.DOPunchScale(Vector3.up*.1f,.5f).AsyncWaitForCompletion();
        return true;
    }

}
