using System.Collections.Generic;
using System.Linq;

using Firebase.Firestore;

[FirestoreData]
public class ConfigModel 
{
    public string docId;
    public string version;
    [FirestoreProperty] public List<BuildingModel> buildings{get;set;}
    [FirestoreProperty] public List<MissionModel> missions{get;set;}

}

[FirestoreData]
public class PopupConfigModel{

    public string docId;
    [FirestoreProperty] public List<PopupModel> popups{get;set;}
}

[FirestoreData]
public class PlayerModel{
    public string docId;
    [FirestoreProperty] public string username{get;set;}

    [FirestoreProperty] public DataModel data{get;set;} = new DataModel();
    [FirestoreProperty] public List<BuildingModel> buildings{get;set;} = new List<BuildingModel>();
    [FirestoreProperty] public List<PropModel> missionProgress{get;set;} = new List<PropModel>();

    //is this needed?
    [FirestoreProperty] public bool tutorialDone{get;set;} = false;


    //[FirestoreProperty] public StatsModel stats{get;set;} = new StatsModel();
    

    public PlayerModel(){
        
    }
    public int GetMissionProgress(string id){
        var mission = missionProgress.FirstOrDefault((x)=>x.key.Equals(id));
        if(mission==null)
            return 0;
        return mission.intValue;
    }
}
[FirestoreData]
public class DataModel{

    //Touch count
    [FirestoreProperty] public int goldTouchCount{get;set;}=0;
    [FirestoreProperty] public int heartTouchCount{get;set;}=0;

    //Stats
    [FirestoreProperty] public int gold{get;set;} =200;
    [FirestoreProperty] public int hearts{get;set;} =100;

    [FirestoreProperty] public List<string> buildings{ get;set;} = new List<string>();
    [FirestoreProperty] public List<PropModel> buildingUpgrades {get;set;} = new List<PropModel>();

    //Building progress
}
[FirestoreData]
public class StatsModel{
    [FirestoreProperty] public int gold{get;set;} =200;
    [FirestoreProperty] public int hearts{get;set;} =100;
}
[FirestoreData]
public class MissionModel{
    [FirestoreProperty] public string docId{get;set;}
    [FirestoreProperty] public string assetId{get;set;}
    [FirestoreProperty] public string title{get;set;}
    
    [FirestoreProperty] public List<PropModel> submissions{get;set;}
}
[FirestoreData]
public class BuildingModel{
    [FirestoreProperty] public string docId{get;set;}    
    [FirestoreProperty] public string assetId{get;set;}
    [FirestoreProperty] public string spriteId{get;set;}
    [FirestoreProperty] public string title{get;set;}
    [FirestoreProperty] public string description{get;set;}

    
    [FirestoreProperty] public CoordModel coord{get;set;}
    [FirestoreProperty] public int cost{get;set;}
    [FirestoreProperty] public int level{get;set;}
    [FirestoreProperty]public int amount{get;set;}
    [FirestoreProperty] public int time{get;set;}
    [FirestoreProperty] public bool autoharvest {get;set;} = false;
    [FirestoreProperty] public Currency currency{get;set;}
    [FirestoreProperty] public List<PropModel> upgrades{get;set;}

}
[FirestoreData]
public class CoordModel{
    [FirestoreProperty] public float x{get;set;}
    [FirestoreProperty] public float y{get;set;}
    [FirestoreProperty] public float z{get;set;}
}


[FirestoreData]
public class PopupModel{
    [FirestoreProperty] public string docId{get;set;}
    [FirestoreProperty] public string assetId{get;set;}
    [FirestoreProperty] public string spriteId{get;set;}
    [FirestoreProperty] public string title{get;set;}
    [FirestoreProperty] public string desc{get;set;}
    [FirestoreProperty] public string nextId{get;set;}
}
[FirestoreData]
public class PropModel{
    [FirestoreProperty] public string key{get;set;}
    [FirestoreProperty] public string title{get;set;}
    [FirestoreProperty] public string docId{get;set;}
    [FirestoreProperty] public int cost{get;set;}
    [FirestoreProperty] public string stringValue{get;set;}
    [FirestoreProperty] public float floatValue{get;set;}
    [FirestoreProperty] public int intValue{get;set;}
    [FirestoreProperty] public Currency currencyValue{get;set;}

    public PropModel(){}
    public PropModel(string key,int intValue=0,string stringValue="",string title="",float floatValue=0){
        this.key=key;
        this.title= title;
        this.stringValue= stringValue;
        this.intValue = intValue;
        this.floatValue = floatValue;
    }
}


public enum Currency{
    GOLD,
    FOOD,
    WOOD,
    HEART
}
