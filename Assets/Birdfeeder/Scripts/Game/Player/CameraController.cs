using UnityEngine;

public class CameraController : MonoBehaviour
{
    public float speed;
    public float zoomSpeed;

    private Vector3 old;
    private Camera cam;
    void Start()
    {
        cam = Camera.main;
    }

    // Update is called once per frame
    void Update()
    {
        if(Input.GetKey(KeyCode.Mouse0))
            GetInput();
        Zoom();
    }

    private void GetInput(){
        var x = Input.GetAxis("Mouse X");
        var z = Input.GetAxis("Mouse Y");

        transform.position-= new Vector3(x,0,z)*speed*Time.deltaTime;
        
    }
    private void Zoom(){
        var delta = Input.GetAxis("Mouse ScrollWheel");
        cam.fieldOfView-=delta*zoomSpeed;
    }

}
