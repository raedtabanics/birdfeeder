using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Linq;

using UnityEngine;

public class Scene : MonoBehaviour
{
    private List<Chunk> chunks = new List<Chunk>();
    private List<BuildingModel> buildings = new List<BuildingModel>();

    private ParticleSystem ps;
    private GameObject spawnCamera;

    public AudioClip spawnSFX;
    public AudioClip chirpSFX;
    void OnEnable(){
        Session.Instance.events.onBuildingUpdate.AddListener( (assetId)=> UpdateLevel());
    }
    async void Start()
    {
        ps = transform.Find("SpawnPS").GetComponent<ParticleSystem>();
        spawnCamera= ps.transform.Find("SpawnCam").gameObject;
        await LoadLevel(Session.Instance.player.buildings);
        foreach(Chunk chunk in chunks)
            chunk.Spawn();
        Session.Instance.events.onSceneReady.Invoke();
    }

    private async Task LoadLevel(List<BuildingModel> toBeBuilt){
        chunks = new List<Chunk>();
        int count =0;
        Debug.Log("Building Count is "+toBeBuilt.Count);
        toBeBuilt.ForEach(async (x)=>{
            var prefab = await Prefabs.GetAddressable<GameObject>(x.assetId);
            Chunk chunk = Instantiate(prefab,new Vector3(x.coord.x,x.coord.y-1,x.coord.z),Quaternion.identity,transform).GetComponent<Chunk>();
            chunks.Add(chunk);
            Building building =chunk.GetComponentInChildren<Building>();
            buildings.Add(x);
            building.Setup(x);
            //await chunk.Spawn();
            count++;
            
        });
        
            
        await Task.Run(async ()=>{
            while(count != toBeBuilt.Count)
                await Task.Delay(25);
            return true;
        });
    }

    private async void UpdateLevel(){

        List<BuildingModel> toBeBuilt = Session.Instance.player.buildings.Where((x)=> !buildings.Any((y)=>y.assetId.Equals(x.assetId))).ToList();
        if(toBeBuilt.Count<=0)
            return;
        var position = toBeBuilt[0].coord;
        await LoadLevel(toBeBuilt);
        
        foreach(Chunk chunk in chunks){

            ps.transform.position = new Vector3(position.x,position.y,position.z);
            spawnCamera.SetActive(true);
            ps.Play();
            Session.Instance.events.onSoundPlay.Invoke(spawnSFX);
            await Task.Delay(1200);
            Session.Instance.events.onSoundPlay.Invoke(chirpSFX);            
            await chunk.Spawn();
            spawnCamera.SetActive(false);
        }
            
    }

}
