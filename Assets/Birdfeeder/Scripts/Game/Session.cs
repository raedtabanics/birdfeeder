using System.Collections.Generic;
using System.Threading.Tasks;

using UnityEngine;
using UnityEngine.Events;

using Firebase;
using Firebase.Auth;
using Firebase.Firestore;
using Firebase.Extensions;
using Newtonsoft.Json;
public class Session : MonoBehaviour
{
    public static Session Instance;
    public SessionEvents events = new SessionEvents();


    //-Database Variables
    public FirebaseAuth Auth;
    public FirebaseUser User;
    public FirebaseFirestore DB;
    public bool firebaseReady = false;
    public string username;

    public TextAsset tempConfig;
    public TextAsset tempPopupConfig;
    public TextAsset playerConfig;
    public ConfigModel config;
    public PopupConfigModel popupConfig;
    public PlayerModel player;

    public bool ready = false;

    async void Awake(){
        if(Instance != null && Instance !=this)
            Destroy(gameObject);
        Instance = this;

        var dependencyStatus = await FirebaseApp.CheckAndFixDependenciesAsync();
        if(dependencyStatus == DependencyStatus.Available)
            InitializeFirebase();
        else
            Debug.Log("[Failed to Connect to Firebase]");
    }

    private void InitializeFirebase(){
            Debug.Log("[Connected to Firebase]");
            Auth = FirebaseAuth.DefaultInstance;          
            DB = FirebaseFirestore.DefaultInstance;
            firebaseReady= true;
    }
    
    async void Start()
    {
        await Task.Run(async ()=>{
            while(!firebaseReady)
                await Task.Delay(25);
            return true;
        });
        Debug.Log("[Firebase Ready]");
        await InjectConfig();
        await InjectPopup();
        await GetConfig();
        await GetPopupConfig();
        ready = true;
    }

    // Update is called once per frame
    void OnDisable()
    {
        //Auth.SignOut();
    }

    private async Task<bool> GetConfig(){
        //config = JsonConvert.DeserializeObject<ConfigModel>(tempConfig.text);
        DocumentReference configRef = Session.Instance.DB.Collection("Config").Document("Alpha");
        DocumentSnapshot snapeShot = await configRef.GetSnapshotAsync();
        if(!snapeShot.Exists)
            return false;
        config = snapeShot.ConvertTo<ConfigModel>();
            
        return true;
    }

    private async Task<bool> GetPopupConfig(){
        //config = JsonConvert.DeserializeObject<ConfigModel>(tempConfig.text);
        DocumentReference configRef = Session.Instance.DB.Collection("Popup").Document("Alpha");
        DocumentSnapshot snapeShot = await configRef.GetSnapshotAsync();
        if(!snapeShot.Exists)
            return false;
        popupConfig = snapeShot.ConvertTo<PopupConfigModel>();
            
        return true;
    }
    public async Task<bool> GetPlayer(){
        //player  = JsonConvert.DeserializeObject<PlayerModel>(playerConfig.text);
        DocumentReference configRef = Session.Instance.DB.Collection("Players").Document(Auth.CurrentUser.UserId);
        DocumentSnapshot snapeShot = await configRef.GetSnapshotAsync();
        if(!snapeShot.Exists)
            return false;
        player = snapeShot.ConvertTo<PlayerModel>();

        Debug.Log(player);
        return true;
    }
    public async Task<bool> InjectConfig(){
        ConfigModel model = JsonConvert.DeserializeObject<ConfigModel>(tempConfig.text);
        await DB.Collection("Config").Document("Alpha").SetAsync(model);
        Debug.Log("config data Injected");
        return true;
    }

    public async Task<bool> InjectPopup(){
        PopupConfigModel model = JsonConvert.DeserializeObject<PopupConfigModel>(tempPopupConfig.text);
        await DB.Collection("Popup").Document("Alpha").SetAsync(model);
        Debug.Log("Popup data Injected");
        return true;
    }
    public async Task<bool> InjectPlayer(PlayerModel model){
        await DB.Collection("Players").Document(Auth.CurrentUser.UserId).SetAsync(model);
        Debug.Log("player data Injected");
        return true;
    }
    
    //Write data
    public async Task<bool> UpdateBuildings(){
        Dictionary<string,object> update =  new Dictionary<string, object>();
        update.Add("buildings",player.buildings);
        await DB.Collection("Players").Document(username).UpdateAsync(update);
        return true;
    }
    public async Task<bool> UpdateData(){
        Dictionary<string,object> update =  new Dictionary<string, object>();
        update.Add("goldTouchCount",player.data.goldTouchCount);
        update.Add("heartTouchCount",player.data.heartTouchCount);
        update.Add("gold",player.data.gold);
        update.Add("hearts",player.data.hearts);
        update.Add("buildings",player.data.buildings);
        await DB.Collection("Players").Document(username).UpdateAsync("data",update);
        return true;
    }

    public async Task<bool> UpdateMissionProgress(){
        Dictionary<string,object> update =  new Dictionary<string, object>();
        update.Add("missionProgress",player.missionProgress);
        await DB.Collection("Players").Document(username).UpdateAsync(update);
        return true;
    }
}

public class SessionEvents{
    //Session Events
    public UnityEvent onReady = new UnityEvent();

    //Game Events
    public UnityEvent onUserAuthenticated = new UnityEvent();
    public UnityEvent onSplashReady = new UnityEvent();
    public UnityEvent onSceneReady = new UnityEvent();

    //Data events
    public UnityEvent<string> onBuildingUpgrade = new UnityEvent<string>();
    public UnityEvent<string> onBuildingUpdate = new UnityEvent<string>();
    public UnityEvent<string> onDataUpdate = new UnityEvent<string>();
    public UnityEvent<string> onMissionUpdate = new UnityEvent<string>();

    //InGame Events
    public UnityEvent<AudioClip> onSoundPlay = new UnityEvent<AudioClip>();
    public UnityEvent<string> onPopupUpdate = new UnityEvent<string>();
}