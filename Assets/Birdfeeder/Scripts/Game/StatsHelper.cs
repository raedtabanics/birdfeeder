using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Linq;

using UnityEngine;

public static class StatsHelper
{
    public static async Task<bool> Buy(BuildingModel model){
        Session.Instance.player.buildings.Add(model);
        Session.Instance.player.data.buildings.Add(model.docId);

        await Session.Instance.UpdateBuildings();
        await Session.Instance.UpdateData();

        Session.Instance.events.onBuildingUpdate.Invoke(model.docId);
        Session.Instance.events.onDataUpdate.Invoke(GlobalVariables.Buildings);
        return true;
    }
    public static async Task<bool> Add(Currency currency,int amount){

        var key = "";
        switch(currency){
            case Currency.GOLD:
                Session.Instance.player.data.goldTouchCount ++;
                Session.Instance.player.data.gold += amount;

                key = Key.GOLD.ToString().ToLower();
                break;
            case Currency.HEART:
                Session.Instance.player.data.heartTouchCount ++;
                Session.Instance.player.data.hearts += amount;

                key = Key.HEARTS.ToString().ToLower();
                break;
            
        }


        await Session.Instance.UpdateData();
        Session.Instance.events.onDataUpdate.Invoke(key);
        return true;
    }

    public static async Task<bool> Remove(Currency curreny, int amount){
        var key ="";

        switch(curreny){
            case Currency.GOLD:
                key = Key.GOLD.ToString().ToLower();
                Session.Instance.player.data.gold -= amount;
                break;
            case Currency.HEART:
                key = Key.HEARTS.ToString().ToLower();
                Session.Instance.player.data.hearts-= amount;
                break;
        }
        
        await Session.Instance.UpdateData();
        Session.Instance.events.onDataUpdate.Invoke(key);
        return true;
    }

    public static async Task<bool> UpdateProgress(string docId, int progress){
        if(Session.Instance.player.missionProgress.Any((x)=>x.key.Equals(docId)))
            Session.Instance.player.missionProgress.FirstOrDefault((x)=>x.key.Equals(docId)).intValue++;
        else
            Session.Instance.player.missionProgress.Add(new PropModel(docId,progress));

        await Session.Instance.UpdateMissionProgress();
        Session.Instance.events.onMissionUpdate.Invoke(docId);
        return true;
    }
}
