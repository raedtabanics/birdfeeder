using System.Collections;
using System.Collections.Generic;

using UnityEngine;
using UnityEngine.UI;

public class BuildingUI : MonoBehaviour
{
    public Image image;
    public Text _name;
    public Button buy;
    public Text cost;
 

    public BuildingModel model;

    public AudioClip cashSFX;
    public async void Setup(BuildingModel model){
        this.model = model;
        _name.text = model.title;
        Sprite sprite = await Prefabs.GetAddressable<Sprite>(model.spriteId);
        image.sprite = sprite;
        cost.text = "BUY " + model.cost.ToString();
        Check();
    }

    public async void Buy(){
        if(!Check())
            return;
        Session.Instance.events.onSoundPlay.Invoke(cashSFX);
        await StatsHelper.Remove(Currency.GOLD,model.cost);
        await StatsHelper.Buy(model);
      
    }

    private bool Check(){
        if(Session.Instance.player.data.gold < model.cost){
            buy.interactable=false;
            return false;
        }
        buy.interactable=true;    
        return true;
    }

}
