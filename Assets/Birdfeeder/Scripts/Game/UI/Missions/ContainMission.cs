using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Linq;

using UnityEngine;

public class ContainMission : MissionItem
{
    protected override async Task<bool> CheckReward(string id){
        if(!model.docId.Equals(id))
            return false;

        var value =(List<string>)prop.GetValue(Session.Instance.player.data);
        if(!value.Any((x)=>x.Equals(current.key)))
            return false;
        
        progress++;
        await StatsHelper.UpdateProgress(model.docId,progress);

        Session.Instance.events.onPopupUpdate.Invoke(current.docId);
        SelectSubMission();
        return true;
    }
}
