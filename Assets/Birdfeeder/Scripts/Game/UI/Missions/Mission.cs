using System.Collections;
using System.Collections.Generic;

using UnityEngine;
using UnityEngine.UI;

public class Mission : MonoBehaviour
{
    private CanvasGroup popup;
    private Button show;
    private Button hide;

    private Transform content;
    private GameObject prefab;
    async void Start()
    {
        popup = transform.Find("Popup").GetComponent<CanvasGroup>();
        show = transform.Find("Show").GetComponent<Button>();
        hide = transform.Find("Popup/Hide").GetComponent<Button>();
        content= transform.Find("Popup/ScrollRect/Viewport/Content");

        

        show.onClick.AddListener(()=>Show());
        hide.onClick.AddListener(()=>Hide());

        Load();
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    private async void Load(){
        foreach(MissionModel model in Session.Instance.config.missions){
            prefab = await Prefabs.GetAddressable<GameObject>(model.assetId);
            MissionItem item = Instantiate(prefab,content).GetComponent<MissionItem>();
            item.Setup(model,Session.Instance.player.GetMissionProgress(model.docId));
        }
    }

    private void Show(){
        popup.blocksRaycasts = true;
        TweenHelper.DoFade(popup.transform,1,.4f);
    }
    private void Hide(){
        popup.blocksRaycasts = false;
        TweenHelper.DoFade(popup.transform,0,.4f);
    }
}
