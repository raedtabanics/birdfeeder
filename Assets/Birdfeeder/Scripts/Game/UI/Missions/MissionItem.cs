using System.Threading.Tasks;
using System.Reflection;

using UnityEngine;
using UnityEngine.UI;

public class MissionItem : MonoBehaviour
{
    //-UI Variables
    public Image icon;
    public Text mainTitle;
    public Text subTitle;
    public Image reward;
    public Text rewardText;
    public Button claim;
    
    //-Logic Variables
    protected MissionModel model;
    protected PropModel current;
    protected PropertyInfo prop;
    protected int progress;


    protected void OnEnable(){
        Session.Instance.events.onDataUpdate.AddListener((id)=>CheckReward(id));
        //Session.Instance.events.onMissionUpdate.AddListener((id)=>UpdateSubmission(id));
    }
    public void Setup(MissionModel model,int progress){
        this.model = model;
        this.progress = progress;
        mainTitle.text = model.title;
        prop = Session.Instance.player.data.GetType().GetProperty(model.docId.ToLower());

        SelectSubMission();
    }

    protected void SelectSubMission(){
        if(progress >= model.submissions.Count){
            //Completed
            return;
        }
            
        current = model.submissions[progress];
        subTitle.text = current.title;
        rewardText.text = current.stringValue;

    }

    //Id == propertiy name
    protected virtual async Task<bool> CheckReward(string id){
        if(!model.docId.Equals(id))
            return false;

        var value =(int)prop.GetValue(Session.Instance.player.data);
        if(value < current.intValue)
            return false;
        
        progress++;
        await StatsHelper.UpdateProgress(model.docId,progress);

        Session.Instance.events.onPopupUpdate.Invoke(current.docId);
        SelectSubMission();
        return true;
    }


}
