using System.Collections;
using System.Threading.Tasks;
using System.Collections.Generic;
using UnityEngine;

public class ValueMission : MissionItem
{
    protected override async Task<bool> CheckReward(string id){
        if(!model.docId.Equals(id))
            return false;

        var value =(int)prop.GetValue(Session.Instance.player.data);
        if(value < current.intValue)
            return false;
        
        progress++;
        await StatsHelper.UpdateProgress(model.docId,progress);

        Session.Instance.events.onPopupUpdate.Invoke(current.docId);
        SelectSubMission();
        return true;
    }
}
