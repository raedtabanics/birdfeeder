using System.Collections;
using System.Collections.Generic;
using System.Linq;

using UnityEngine;
using UnityEngine.UI;

public class OwnedBuildingUI : MonoBehaviour
{
    public Image image;
    public Text title;
    public Text cost;
    public Button upgrade;

    private BuildingModel refrence;
    private BuildingModel model;
    private PropModel nextUpgrade;
    
    public AudioClip cashSFX;
    public async void Setup(BuildingModel model){
        this.model = model;
        Sprite sprite = await Prefabs.GetAddressable<Sprite>(model.spriteId);
        image.sprite = sprite;
        refrence = Session.Instance.config.buildings.FirstOrDefault((x)=>x.assetId.Equals(model.assetId));
        if(refrence == null)
            return;
        Refresh();
        
    }

    public async void Upgrade(){        
        if(nextUpgrade == null)
            return;
        //Could use Properties

        switch(nextUpgrade.key){
            case "amount":
                model.amount += nextUpgrade.intValue;
                break;
            case "auto":
                model.autoharvest = true;
                break;
            default:
                break;
        }
        model.level ++;
        Session.Instance.events.onSoundPlay.Invoke(cashSFX);
        await StatsHelper.Remove(Currency.GOLD,nextUpgrade.cost);
        Debug.Log("Subtracting cost");
        await Session.Instance.UpdateBuildings();
        Session.Instance.events.onBuildingUpgrade.Invoke(model.assetId);
        
        nextUpgrade = null;
        Refresh();
    }

    private void Refresh(){
        if(model.level >= refrence.upgrades.Count)
            return;
        nextUpgrade = model.upgrades[model.level];
        title.text =  nextUpgrade.stringValue;
        cost.text = "Upgrade " + nextUpgrade.cost;
        Check();
    }

    private bool Check(){
        Debug.Log("Checking");
        if(nextUpgrade == null || Session.Instance.player.data.gold<= nextUpgrade.cost){
            
            upgrade.interactable=false;
            return false;
        }
        upgrade.interactable=true;    
        return true;
    }
}
