using System.Threading.Tasks;

using UnityEngine;
using UnityEngine.UI;

using DG.Tweening;
public class Popup : MonoBehaviour
{
    public Image icon;
    public Text text;
    public Button button;

    protected PopupModel model;

    public AudioClip sfx; //ToBe relaced
    // Start is called before the first frame update
    public async Task<bool> Setup(PopupModel model)
    {
        this.model = model;
        icon.sprite = await Prefabs.GetAddressable<Sprite>(model.spriteId);
        //text.text = model.desc;
        return true;

    }

    public async Task<bool> Show(){
        await transform.DOScale(Vector3.one,.3f).AsyncWaitForCompletion();
        //await transform.DOShakeScale(.1f).AsyncWaitForCompletion();
        icon.transform.DOBlendableScaleBy(Vector3.up*.2f,.1f).SetEase(Ease.InOutSine).SetLoops(12,LoopType.Yoyo);
        Session.Instance.events.onSoundPlay.Invoke(sfx);
        await DisplayMsg(model.desc);
        
        return true;
    }
    protected async Task<bool> DisplayMsg(string msg){
        text.text ="";
        foreach(char c in msg){
            if(Input.GetKey(KeyCode.Mouse0))
                break;
            text.text +=c;
            await Task.Delay(40);
        }
        text.text =msg;
        return true;
    }

    public async Task<bool> Hide(){

        await transform.DOScale(Vector3.zero,.3f).AsyncWaitForCompletion();
        return true;
    }
    
}
