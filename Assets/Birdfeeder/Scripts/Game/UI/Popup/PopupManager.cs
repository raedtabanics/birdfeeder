using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using UnityEngine;

public class PopupManager : MonoBehaviour
{
    public List<PopupModel> popups = new List<PopupModel>();
    private GameObject current;

    private PopupModel model;
    private Popup popup;

    void OnEnable(){
        Session.Instance.events.onPopupUpdate.AddListener(async (docId)=>await Show(docId));
    }
    async void Start()
    {
        if(Session.Instance.player.tutorialDone)
            return;
        await Task.Delay(2000);
        await Show("START_TUTORIAL");
    }
    // Update is called once per frame

    public async Task<bool> Setup(string docId){

        model = Session.Instance.popupConfig.popups.FirstOrDefault((x)=>x.docId.Equals(docId));
        if(model == null)
            return true;

        var prefab = await Prefabs.GetAddressable<GameObject>(model.assetId);
        current = Instantiate(prefab,transform);
        popup = current.GetComponent<Popup>();
        await popup.Setup(model);
        popup.button.onClick.AddListener(async ()=>await Show(model.nextId));
        return true;
    }   

    private async Task<bool> Show(string docId){
        await Hide();

        if(docId == null || docId.Equals(""))
            return true;
        await Setup(docId);
        await popup.Show();

        return true;
    }

    private async Task<bool> Hide(){
        if(current==null)
            return true;

        await popup.Hide();
        Destroy(popup.gameObject);
        current = null;
        return true;
    }

}
