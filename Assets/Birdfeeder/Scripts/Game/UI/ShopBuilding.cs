using System.Linq;
using System.Threading.Tasks;
using System.Collections.Generic;

using UnityEngine;
using UnityEngine.UI;

public class ShopBuilding : MonoBehaviour
{
    private Transform popup;
    private Transform content;
    private Button show;
    private Button hide;

    void Start()
    {
        popup = transform.Find("Popup");
        content = popup.Find("Viewport/Content");
        show = transform.Find("Show").GetComponent<Button>();
        hide = popup.Find("Hide").GetComponent<Button>();

        show.onClick.AddListener(()=>Show());
        hide.onClick.AddListener(()=>Hide());
    }


    private async Task<bool> Load(){
        foreach(Transform child in content)
            Destroy(child.gameObject);

        var ownedPrefab = await Prefabs.GetAddressable<GameObject>("OwnedBuildingUI");
        Session.Instance.player.buildings.ForEach((x)=>{
            OwnedBuildingUI item = Instantiate(ownedPrefab,content).GetComponent<OwnedBuildingUI>();
            item.Setup(x);
        });

        var prefab = await Prefabs.GetAddressable<GameObject>("ToBeBuiltBuildingUI");
        List<BuildingModel> toBeBuilt = Session.Instance.config.buildings.Where((x)=>!Session.Instance.player.buildings.Any((y)=>y.assetId.Equals(x.assetId))).ToList();
        toBeBuilt.ForEach((x)=>{
            BuildingUI item = Instantiate(prefab,content).GetComponent<BuildingUI>();
            item.Setup(x);
        });

        return true;
    }

    public async void Show(){
        await Load();
        TweenHelper.DoAnchorSlideX(popup,0,.5f);
    }
    public void Hide(){
        TweenHelper.DoAnchorSlideX(popup,700,.5f);
    }
}
