using System.Linq;
using System.Reflection;

using UnityEngine;
using UnityEngine.UI;

using DG.Tweening;
[RequireComponent(typeof(CanvasGroup))]
public class DynamicStats : MonoBehaviour
{
    //-Data Variables
    public string assetId; //Building Dependency 
    //public string key; // State Dependency 
    public Key key;

    //-UI Variables
    private CanvasGroup cg;
    private Text text;
    private Image icon;

    //-Logic Variables
    public float duration = .7f;
    private PropertyInfo stat;
    private int count=0;
    private int current;
    private bool ready = false;

    void OnEnable(){
        Session.Instance.events.onBuildingUpdate.AddListener((assetId)=>Show(assetId));
        Session.Instance.events.onDataUpdate.AddListener((id)=>UpdateStats(id));
    }
    void Start()
    {
        cg = GetComponent<CanvasGroup>();
        text = transform.Find("Text").GetComponent<Text>();
        icon = transform.Find("Image").GetComponent<Image>();

        Debug.Log(key.ToString());

        stat = Session.Instance.player.data.GetType().GetProperty(key.ToString().ToLower());
        current =(int)stat.GetValue( Session.Instance.player.data);
        UpdateStats(key.ToString().ToLower());

        var model = Session.Instance.player.buildings.FirstOrDefault((x)=>x.assetId.Equals(assetId)); 
        if(assetId == "" || model != null)
            Show(assetId);
        //Show(assetId);
    }

    private void Show(string assetId){
        Debug.Log("Showing for " + assetId);
        if(!this.assetId.Equals(assetId))
            return;
        cg.DOFade(1,.7f);
    }
    private void UpdateStats(string id){
        if(!id.Equals(key.ToString().ToLower()))
            return;

        current =(int)stat.GetValue( Session.Instance.player.data);
        if(count == current)
            return;
               
        DOTween.To(() => count, x => count = x, current, duration)
        .OnUpdate(() => {
            text.text = count.ToString();
        });
    }

}
